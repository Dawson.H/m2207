package tp4;

import java.util.Scanner;

public class CombatAvecJoueur {

	public static void main(String[] args) {
		//Instantiation
		Scanner sc = new Scanner(System.in);
		
		//Demander une chaine de caract�re � l'utilisateur
		System.out.println("Saisir le nom du premier combattant :");
		String poke1 = sc.nextLine();
		
		System.out.println("Saisir le nom du second combattant :");
		String poke2 = sc.nextLine();
		
		//D�claration
		Pokemon p1,p2;
		
		//Instantiation
		p1=new Pokemon(poke1);
		p2=new Pokemon(poke2);
		
		
		//Combat:
		int round=1;
		
		while (p1.isAlive()==true && p2.isAlive()==true) {
		System.out.println("Round "+ round);
		p1.sePresenter(); p2.sePresenter();
		
			//Attaquer ou manger p1
			p1.sePresenter();
			System.out.println(p1.getNom()+" Attaquer(0) ou Manger(1) ?");
			int pk1=sc.nextInt();
			if (pk1==0) {
				p1.attaquer(p2);
			}
			else {
				p1.manger();
			}
			
			//Attaquer ou manger p2
			p2.sePresenter();
			System.out.println(p2.getNom()+" Attaquer(0) ou Manger(1) ?");
			int pk2=sc.nextInt();
			if (pk2==0) {
				p2.attaquer(p1);
			}
			else {
				p2.manger();
			}
			round = round +1;
	}
		if (p1.isAlive()==true){
			System.out.println("Pikamachin est le vainqueur");
		}
		else if (p2.isAlive()==true) {
			System.out.println("Salafleche est le vainqueur");
		}
		else {
			System.out.println("Egalit�");
		}
	}
}
