package tp4;

public class TestPokemon {

	public static void main(String[] args) {
		//Declaration
		Pokemon pika;
		Pokemon sala;

		//Instantiation
		pika=new Pokemon("Pikamachin");
		sala=new Pokemon("Salafleche");

		//Test
		pika.sePresenter();
		pika.perdreEnergie(4);
		pika.sePresenter();
		sala.sePresenter();
		pika.attaquer(sala);
		sala.sePresenter();

	}

}
