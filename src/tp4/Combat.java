package tp4;

public class Combat {

	public static void main(String[] args) {
		//Declaration
		Pokemon pika, sala;

		//Instantiation
		pika=new Pokemon("Pikamachin");
		sala=new Pokemon("Salafleche");

		int round=1;
		while (pika.isAlive()==true && sala.isAlive()==true) {
			
			System.out.println("Round "+ round);
			pika.sePresenter();
			pika.attaquer(sala);
			sala.sePresenter();
			sala.attaquer(pika);
			pika.sePresenter();
			round=round+1;
		}
		if (pika.isAlive()==true){
			System.out.println("Pikamachin est le vainqueur");
		}
		else if (sala.isAlive()==true) {
			System.out.println("Salafleche est le vainqueur");
		}
		else {
			System.out.println("Egalit�");
		}

	}
}
