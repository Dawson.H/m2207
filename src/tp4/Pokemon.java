package tp4;

public class Pokemon {
	//Attributs
	private int energie, maxEnergie, puissance;
	private String nom;

	//Accesseurs
	public String getNom() {
		return nom;
	}

	public int getEnergie() {
		return energie;
	}
	public int getPuissance() {
		return puissance;
	}

	//Constucteur
	public Pokemon(String nom) {
		this.nom=nom;
		maxEnergie=50 + (int) (Math.random()*((90-50)+1));
		energie=30 + (int)(Math.random()*((maxEnergie-30)+1));
		puissance=3 + (int)(Math.random()*((10-3)+1));
	}

	//Methodes
	public void sePresenter() {
		System.out.println("Je suis "+ nom + ", j'ai " + energie + " points d'�nergie. Max " + maxEnergie + " et une puissance de " + puissance);
	}

	public void manger() {
		energie= energie + (10+(int)(Math.random()*((30-10)+1)));
		if(energie>maxEnergie) {
			energie= maxEnergie;
		}
		sePresenter();
		System.out.println(nom+" n'a plus faim, "+nom+" jette la nouritture");
	}

	public void vivre(){
		energie = energie - (20+(int)(Math.random()*((40-20)+1)));
		sePresenter();
	}

	public boolean isAlive() {
		if (energie>0) {
			return true;
		}
		else {
			return false;
		}
	}

	public void cycleVie() {
		int cycle=0;
		while (isAlive()==true) {
			manger();
			vivre();
			cycle = cycle + 1;
			System.out.println(nom+" a v�cu "+cycle+" cycle !");
		}
	}
	public void perdreEnergie(int perte) {
		if (energie<2.5*maxEnergie) {
			energie=(int) (energie-1.5*perte);
		}
		else {
			energie=energie-perte;
		}
	}
	
	public void attaquer(Pokemon adversaire) {
		System.out.println(nom+" attaque et inflige "+ puissance+" degats.");
		if (puissance>1) {
			puissance = puissance - (0+(int)(Math.random()*((1-0)+1)));
		}
		if (energie<2.5*maxEnergie) {
			puissance = 2*puissance;
		}
		adversaire.perdreEnergie(puissance); 
	}
}
