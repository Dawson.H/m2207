package tp1;

public class MaBanque {

	public static void main(String[] args) {
		//Declaration
		Compte compte1, c2;
		
		//Instanciation
		compte1= new Compte(1);
		c2= new Compte(2);
		
		//Affichage
		System.out.println("Votre decouvert est de: "+compte1.getDecouvert());
		compte1.setDecouvert(100);
		System.out.println("Votre decouvert est de: "+compte1.getDecouvert());
		System.out.println("Votre solde est de: "+compte1.getSolde());
		compte1.depot(100);
		System.out.println("Votre solde est de: "+compte1.getSolde());
		
		//Cycle complet
		System.out.println("Exercice 2.6");
		c2.depot(1000);
		c2.afficherSolde();
		System.out.println("Votre decouvert est de: "+c2.getDecouvert());
		c2.retrait(600);
		c2.afficherSolde();
		System.out.println("Votre decouvert est de: "+c2.getDecouvert());
		c2.retrait(700);
		c2.afficherSolde();
		System.out.println("Votre decouvert est de: "+c2.getDecouvert());
		c2.setDecouvert(500);
		System.out.println("Votre decouvert est de: "+c2.getDecouvert());
		c2.afficherSolde();
		c2.retrait(700);
		
	}

}
