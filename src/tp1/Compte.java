package tp1;

public class Compte {
	private int numero;
	private double solde, decouvert;

	//constructeur
	public Compte(int numero){
		this.numero=numero;
		solde=0;
		decouvert=0;
	}
	//Accesseur
	public void setDecouvert(double montant) {
		decouvert=montant;
	}
	public double getDecouvert() {
		return decouvert;
	}
	public int getNumero() {
		return numero;
	}
	public double getSolde() {
		return solde;
	}

	//Methodes
	public void afficherSolde() {
		System.out.println("Votre solde s'�l�ve �: "+solde);
	}
	
	public void depot(double montant) {
		this.solde= montant+solde;
	}
	
	public String retrait(double montant) {
		if (montant>(this.solde+decouvert)) {
			return "Retrait refus�";
		}
		else {
			this.solde = solde-montant;
			return "Retrait effectu�";
		}
	}
}
