package tp1;

public class Client {
	//Attributs
	String nom;
	String prenom;
	Compte compteclient;
	
	//Constructeur
	public Client(String n, String p){
		nom=n;
		prenom=p;
	}
	public Client(String n, String p, Compte c){
		nom=n;
		prenom=p;
		compteclient=c;
	}
	
	
	public Client() {
		nom="";
		prenom="";
	}
	
	//Accesseurs
	public String getX(){
		return nom;
	}
	public String getY(){
		return prenom;
	}
	public Compte getZ(){
		return this.compteclient;
	}
}
