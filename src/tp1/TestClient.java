package tp1;

public class TestClient {

	public static void main(String[] args) {
		//Declaration
		Client client1, client2;
		Compte compte1, compte2;
		
		//Instantiation
		client1=new Client("Hoareau", "Dawson");
		compte1=new Compte(1); 
		compte2=new Compte(2);
		client2=new Client("Bob", "Lenon", compte1);
		
		//Affichage
		System.out.println("Nom="+client1.getX());
		System.out.println("Prenom="+client1.getY());
		System.out.println("Nom="+client1.getX()+" Prenom="+client1.getY()+" Compte="+client2.getZ());
	}

}
