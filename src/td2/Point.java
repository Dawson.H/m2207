package td2;

public class Point {
	//Attributs
	public int x, y;
	
	//Constructeur
	public Point(int x, int y) {
		this.x=x;
		this.y=y;
	}
	//Accesseurs
		public int getX() {
			return x;
	}
		public void setX(int x) {
			this.x=x;
		}
		public int getY() {
			return y;
		}
		public void setY() {
			this.y=y;
		}
		public void seDecrire() {
			System.out.println("Les coordonnées sont x="+x+" et y="+y);
		}
		public void resetcoordonnees() {
			this.x=0;
			this.y=0;
		}
}
