package tp3;

public class Prison {
	//Attributs
	private String nom;
	private Brigand tab[]={};
	private int nbbrigand;
	
	//Constructeur
	public Prison(String nom) {
		this.nom=nom;
	}
	
	//Methodes
	public void mettreEnCellule(Brigand brigand) {
			System.out.println(brigand+" est mis dans la cellule"+tab[10]);
	}
	public void sortirDeCellule(Brigand brigand) {
		System.out.println(brigand+" est sorti de sa cellule");
	}
	public void compterLesPrisonniers() {
		System.out.println("Il y � "+nbbrigand+" dans la prison "+nom);
	}
	
}
