package tp3;

public class Dame extends Humain{
	//Attributs
	private boolean libre;

	//Constucteur
	public Dame(String nom) {
		super(nom);
		libre=true;
		boisson="le Martini"; 
	}

	//Methodes
	public void priseEnOtage() {
		libre=false;
		parler("Au secours");
	}


	public void estLiberee() {
		libre=true;
		parler("Merci Cowboy");
	}


	public String quelEstTonNom() {
		return "je suis miss "+ nom;
	}


	public void sePresenter() {
		boisson="Wiskey";
		super.sePresenter();
		if (libre==true) {
			parler("Actuellement, je suis libre");
		}
		else {
			parler("Actuellement, je suis kidnapp�e");
		}
	}
}
