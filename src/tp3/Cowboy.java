package tp3;

public class Cowboy extends Humain{
	//Attributs
	private int popularite;
	private String caracteristique;

	//Constructeurs
	public Cowboy(String nom) {
		super(nom);
		boisson="le Wiskey";
		popularite=0;
		caracteristique="Vaillant";
	}

	//Methodes
	public void tire(Brigand brigand) {
		System.out.println("Le "+caracteristique+" "+nom+" tire sur "+ brigand.nom +". PAN !");
		parler("Prends �a, voyou !");
	}

	public void libere(Dame dame) {
		dame.estLiberee();
		popularite=popularite+10;
	}

}
