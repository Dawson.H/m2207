package tp3;

public class Sherif extends Cowboy{

	//Attributs
	private int nbdebrigand;

	//Constructeurs
	public Sherif(String nom) {
		super(nom);
	}

	//Methodes
	public String quelEstTonNom() {
		return "Sherif "+nom;		
	}

	public void sePresenter() {
		parler(quelEstTonNom()+" J'ai emprison� "+nbdebrigand+" brigand");
	}
	public void coffrer(Brigand brigand, Sherif sherif) {
		brigand.emprisonner(sherif);;
		nbdebrigand++;
		parler("Au nom de la loi, "+brigand.nom+", je vous arr�te.");
	}
}
