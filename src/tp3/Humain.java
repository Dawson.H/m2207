package tp3;

public class Humain {
	//Attributs
	protected String nom, boisson;

	//Constructeur
	public Humain(String nom) {
		this.nom=nom;
		this.boisson="le lait";
	}

	//Methodes
	public String quelEstTonNom() {
		return "Je suis "+nom;
	}
	public String quelEstTaBoisson() {
		return "Ma boisson favorite "+boisson;
	}
	public void parler(String texte) {
		System.out.println(nom + "- "+texte);
	}
	public void sePresenter() {
		parler("Bonjour, "+quelEstTonNom()+" et "+quelEstTaBoisson());
	}
	public void boire() {
		parler("Ah ! un bon verre de "+ quelEstTaBoisson()+"! GLOUPS !");
	}

}
