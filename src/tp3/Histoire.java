package tp3;

public class Histoire {

	public static void main(String[] args) {
		//Declaration
		Humain Bob;
		Dame Alice;
		Brigand Django;
		Cowboy Bebop, Clint;
		Sherif Dan;
		Prison Alamo;

		//Instanciation
		Bob=new Humain("Bob");
		Alice= new Dame("Alice");
		Django=new Brigand("Django");
		Bebop=new Cowboy("Bebop");
		Dan=new Sherif("Dan");
		Clint=new Sherif("Clint");
		Alamo=new Prison("Alamo");
		

		
		//TEST
		Bob.sePresenter();
		Bob.boire();
		Alice.sePresenter();
		Alice.priseEnOtage();
		Alice.estLiberee();
		Django.sePresenter();
		Bebop.sePresenter();
		Django.enlever(Alice);
		Bebop.tire(Django);
		Bebop.libere(Alice);
		Dan.quelEstTonNom();
		Dan.sePresenter();
		Dan.coffrer(Django, Dan);
		Clint.sePresenter();
		Alamo.mettreEnCellule(Django);
		Alamo.compterLesPrisonniers();
		Alamo.sortirDeCellule(Django);
		Alamo.compterLesPrisonniers();
		
		

		/*
		//Sc�nario 1
		System.out.println("Premier sc�nario");
		Alice.sePresenter();
		Django.sePresenter();
		Django.enlever(Alice);
		Django.sePresenter();
		Alice.sePresenter();
		Bebop.tire(Django);
		Bebop.libere(Alice);
		Alice.sePresenter();
		*/
		
		/*
		//Sc�nario 2
		System.out.println("Second sc�nario");
		Dan.sePresenter();
		Dan.coffrer(Django, Dan);
		Dan.sePresenter();
		*/
		
		
		
	}

}
