package tp3;

public class Brigand extends Humain{
	//Attributs
	private String look;
	private int nombrededames, recompense;
	private boolean prison;

	//Constructeurs
	public Brigand(String nom) {
		super(nom);
		look="mechant";
		prison=false;
		recompense=100;
		nombrededames=0;
		boisson="le Cognac";
	}

	//Methodes
	public int getRecompense() {
		return recompense;
	}

	public String quelEstTonNom() {
		return "Je suis "+nom+" le "+look;
	}

	public void sePresenter() {
		super.sePresenter();
		parler("J'ai l'air "+look+" et j'ai enlev� "+nombrededames);
		parler("Ma t�te est mise � prix "+recompense+"$");
	}
	public void enlever(Dame dame) {
		nombrededames++;
		parler("Ah ah "+dame.nom+". Tu es ma prisonni�re !");
		dame.priseEnOtage();
	}
	public void emprisonner(Sherif sherif) {
		parler("Damned ! je suis fait. "+sherif.nom+", tu m'as eu.");
	}
}
