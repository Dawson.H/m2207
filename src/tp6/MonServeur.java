package tp6;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class MonServeur{
	public static void main(String[] args) {
		ServerSocket monServerSocket; //Creation d'un serveur socket
		Socket monSocketClient; //Creation d'un socket client
		BufferedReader monBufferedReader;//Cr�ation du bufferreader
		
		try {
			
			monServerSocket = new ServerSocket(8888); //Attribution du port 8888 au serveur socket
			System.out.println("ServerSocket: " + monServerSocket);
			monSocketClient = monServerSocket.accept();
			System.out.println("Le client s'est connect�");
			monBufferedReader = new BufferedReader(new InputStreamReader(monSocketClient.getInputStream()));
			
			String message = monBufferedReader.readLine();
			System.out.println("Message : " + message);
			
			monSocketClient.close();//Fermeture du socket client
			monServerSocket.close();//Fermeture du socket serveur
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
