package tp6;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import tp5.MonAppliGraphique;

public class PanneauServeur extends JFrame implements ActionListener{
	ServerSocket serveur;// Creation du Serveur SOCKET
	Socket client; //Creation d'un socket client

	Container panneau = getContentPane();//Creation de container
	JButton bquitter= new JButton("Quitter");//Creation d'un boutton
	JTextArea panneauActif = new JTextArea(" ");//Creation d'un champ texte




	public PanneauServeur() {//Constructeur panneau
		super();
		setTitle("Serveur - Panneau d'affichage");
		this.setSize(400,300);
		this.setLocation(20,20);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);

		panneauActif.append("Le Panneau est Actif \n ");
		panneau.add(bquitter, BorderLayout.SOUTH);
		panneau.add(panneauActif, BorderLayout.CENTER);
		bquitter.addActionListener(this);

		try {
			serveur = new ServerSocket(8888); //Attribution du port 8888 au serveur socket
			panneauActif.append("Serveur d�marr� \n");
			client = serveur.accept();
			serveur.close();
		}	
		catch (Exception e) {
			e.printStackTrace();
			panneauActif.append("Erreur de cr�ation du socketServeur");
		}

		ecouter();
	}
	public void ecouter() {
		panneauActif.append("En attente de connexion d'un client... \n");
		try {
			panneauActif.append("Client Connect�\n");
			BufferedReader buffer;//Cr�ation du bufferreader
			String ligne;
			while ((ligne = buffer.readLine()) != null){
				panneauActif.append("Message recu : " + ligne + "\n");
			}
			catch(Exception e) {
				e.printStackTrace();
				panneauActif.append("Erreur de cr�ation du socketServeur");
			}



		}
	}
	public static void main(String[] args) {	//Creation de la fenetre
		PanneauServeur app = new PanneauServeur ();	
	}

	public void actionPerformed(ActionEvent arg0) { //Quitter avec le boutton
		System.exit(-1);
	}
}
