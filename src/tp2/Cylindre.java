package tp2;

public class Cylindre extends Cercle{
	//Attributs
	private double hauteur;
	
	//Constructeur
	public Cylindre() {
		hauteur=1.0;
	}
	public Cylindre(double h, double rayon, String couleur, boolean coloriage) {
		super (rayon, couleur, coloriage);
		hauteur=h;
	}
	
	//Accesseurs
	public double getX() {
		return hauteur;
	}
	public void setX(double h) {
		hauteur=h;
	}
	
	//Methodes
	public String seDecrire() {
		return "Un cylindre de hauteur "+hauteur+" issue d'"+super.seDecrire();
	}
	public double calculerVolume() {
		return hauteur*super.calculerAire();
	}
	
}
