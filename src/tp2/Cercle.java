package tp2;

public class Cercle extends Forme{

	//Attributs
	private double rayon;

	//Accesseurs
	public double getX() {
		return rayon;
	}
	public void setX(double r) {
		rayon=r;
	}

	//Constructeurs
	public Cercle() {
		super();
		rayon=1.0;
	}
	public Cercle(double r){
		/*Comme le constructeur précedent initialise le Cercle() avec super(); il n'ets pas utile de
		 * le re-préciser ici*/
		rayon=r;
	}
	public Cercle(double r, String couleur, boolean coloriage) {
		super(couleur, coloriage);
		rayon=r;		
	}

	//Methodes
	public String seDecrire() {
		return "Un cercle de rayon "+rayon+" est issue d'"+super.seDecrire(); 
	}
	public double calculerAire() {
		return Math.PI*(rayon*rayon);
	}
	public double calculerPerimetre() {
		return 2*rayon*Math.PI;
	}
}
