package tp2;

public class Forme {
	//Attributs
	private static int nombre=0;
	private String couleur;
	private boolean coloriage;

	//Constructeurs
	public Forme() {
		nombre++;
		couleur = "orange";
		coloriage = true;
	}
	public Forme(String c, boolean r) {
		nombre++;
		couleur=c;
		coloriage=r;		
	}

	//Accesseurs
	public String getCouleur() {
		return couleur;
	}
	public void setCouleur(String c) {
		couleur=c;
	}
	public boolean isColoriage() {
		return coloriage;
	}
	public void setColoriage(boolean b) {
		coloriage=b;
	}

	//M�thodes
	public String seDecrire() {
		return "Une forme de couleur "+ couleur+" et de coloriage "+ coloriage;	
	}
	public static int nbObjet() {
		return nombre;
	}

}
