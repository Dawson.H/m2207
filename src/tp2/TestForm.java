package tp2;

public class TestForm {

	public static void main(String[] args) {
		//Declaration Forme
		Forme f1, f2;
		
		//Declaration Cercle
		Cercle c1, c2, c3;
		
		//Declaration Cylindre
		Cylindre cy1, cy2;
		
		//Instanciation Forme
		f1=new Forme();
		f2=new Forme("vert", false);
		
		//Instanciation Cercle
		c1=new Cercle();
		c2=new Cercle(2.5);
		c3=new Cercle(3.2, "jaune", false);
		
		//Instanctiation Cylindre
		cy1=new Cylindre();
		cy2=new Cylindre(4.2,1.3,"bleu",true);
		
		//Affichage
		System.out.println(Forme.nbObjet()+" de types formes � �t� cr�e");
		
		//Affichage Forme
		System.out.println(f1.getCouleur()+" "+f1.isColoriage());
		System.out.println(f1.seDecrire());

		System.out.println(f2.getCouleur()+" "+f2.isColoriage());
		System.out.println(f2.seDecrire());

		f1.setCouleur("rouge");
		f1.setColoriage(false);
		System.out.println(f1.getCouleur()+" "+f1.isColoriage());
		System.out.println(f1.seDecrire());
		
		//Affichage Cercle
		System.out.println("Classe du Cercle");
		System.out.println(c1.seDecrire());
		System.out.println(c2.seDecrire());
		System.out.println("Question2.10");
		System.out.println(c3.seDecrire());
		System.out.println("Question2.11");
		System.out.println("Aire="+c2.calculerAire()+" Perimetre= "+c2.calculerPerimetre());
		System.out.println("Aire="+c3.calculerAire()+" Perimetre= "+c3.calculerPerimetre());
		
		//Affichage Cylindre
		System.out.println("Classe du Cylindre");
		System.out.println(cy1.seDecrire());
		System.out.println("Question 3.4");
		System.out.println(cy2.seDecrire());
		System.out.println("Question 3.5");
		System.out.println("Volume="+cy1.calculerVolume());
		System.out.println("Volume="+cy2.calculerVolume());
		
		System.out.println(Forme.nbObjet()+" de types formes � �t� cr�e");
	}

}
