package tp5;

import javax.swing.JFrame;
import javax.swing.JButton;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JTextField;

public class MonAppliGraphique extends JFrame{
	
	//Creation du boutton
	JButton b1= new JButton("Boutton 1");
	JButton b2= new JButton("Boutton 2");
	JButton b3= new JButton("Boutton 3");
	JButton b4= new JButton("Boutton 4");
	JButton b5= new JButton("Boutton 5");
	
	JLabel monLabel = new JLabel("Je suis un JLabel");
	JTextField monTextField = new JTextField("Je suis un JTextField");

	
	//Creation du container
	Container panneau = getContentPane();
	
	//Constructeur
	public MonAppliGraphique() {
		super();
		setTitle("Ma premi�re application");
		this.setSize(400,200);
		this.setLocation(20,20);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		
		//panneau.setLayout(new FlowLayout());
		panneau.setLayout(new GridLayout(3,2));
		
		panneau.add(b1, BorderLayout.NORTH);
		panneau.add(b2, BorderLayout.CENTER);
		panneau.add(b3, BorderLayout.SOUTH);
		panneau.add(b4, BorderLayout.EAST);
		panneau.add(b5, BorderLayout.WEST);
		
		panneau.add(monLabel, BorderLayout.EAST);
		panneau.add(monTextField, BorderLayout.WEST);
		
		System.out.println("La fen�tre est cr��e !");
	}

	//Methodes Main
	public static void main(String[] args) {
		MonAppliGraphique app = new MonAppliGraphique ();	
	}
}
