package tp5;

import javax.swing.JFrame;
import javax.swing.JButton;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.*;

import javax.swing.JLabel;
import javax.swing.JTextField;


public class CompteurClic extends JFrame implements ActionListener{
	int nombreclic=0;
	
	//Creation du boutton
	JButton b1= new JButton("Click !");
	JLabel monLabel = new JLabel("Vous avez cliquez "+nombreclic+" fois");
	
	//Creation du container
	Container panneau = getContentPane();
	
	//Constructeur
	public CompteurClic() {
		super();
		setTitle("Compteur Clic");
		this.setSize(200,100);
		this.setLocation(20,20);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		
		//Panneau
		panneau.setLayout(new FlowLayout());
		panneau.add(b1, BorderLayout.NORTH);
		panneau.add(monLabel, BorderLayout.SOUTH);
		
		b1.addActionListener(this);

		System.out.println("La fen�tre est cr��e !");	
	}

	//Methodes Main
	public static void main(String[] args) {
		CompteurClic app = new CompteurClic();	
	}

	public void actionPerformed(ActionEvent arg0) {
		System.out.println("Une action a �t� d�tect�e");
		this.nombreclic=nombreclic+1;
		monLabel.setText("Vous avez cliquez "+nombreclic+" fois");
	}
}
